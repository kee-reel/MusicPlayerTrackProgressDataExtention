#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_music_player_track_progress_data_extention.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IMusicPlayerTrackProgressDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IMusicPlayerTrackProgressDataExtention IMusicPlayerTrackDataExtention IDataExtention)

	DATA_EXTENTION_BASE_DEFINITIONS(IMusicPlayerTrackProgressDataExtention, IMusicPlayerTrackDataExtention, {"progress"})

public:
	DataExtention(QObject* parent) :
		QObject(parent)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

public:
	QString name() override
	{
		return "New Task";
	}

	QString path() override
	{
		return "";
	}

	int progress() override
	{
		return 0;
	}
};
